# frozen_string_literal: true

require 'date'
require_relative 'spec_helper'
require_relative '../app/model'
require_relative '../app/model_serializer'

DifferentModel = Struct.new(:id, :name, :extra_attribute)

RSpec.describe ModelSerializer do
  subject { described_class.new(model) }

  let(:model) do
    Model.new(1, 'Nevara')
  end

  it 'serializes object' do
    expect(subject.serialize).to eq({
                                      id: 1,
                                      name: 'Nevara'
                                    })
  end

  context 'for different objects' do
    let(:model) do
      DifferentModel.new(2, 'SomeModel', 'new attribute')
    end

    it 'does not serialize attributes not defined in serializer' do
      expect(subject.serialize.keys).not_to include(:extra_attribute)
    end
  end
end
