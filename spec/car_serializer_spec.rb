# frozen_string_literal: true

require 'date'
require_relative 'spec_helper'
require_relative '../app/car'
require_relative '../app/car_serializer'

DifferentCar = Struct.new(:id, :name, :created_at, :extra_attribute)

RSpec.describe CarSerializer do
  subject { described_class.new(car) }

  let(:car) do
    Car.new(1, 'Rimac', Date.new(2021, 1, 1))
  end

  it 'serializes object' do
    expect(subject.serialize).to eq({
                                      id: 1,
                                      name: 'Rimac',
                                      created_at: '01-01-2021'
                                    })
  end

  context 'for different objects' do
    let(:car) do
      DifferentCar.new(2, 'SomeCar', Date.new(2021, 1, 1), 'new attribute')
    end

    it 'does not serialize attributes not defined in serializer' do
      expect(subject.serialize.keys).not_to include(:extra_attribute)
    end
  end
end
