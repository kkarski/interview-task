# frozen_string_literal: true

require_relative 'serializer'

class ModelSerializer < Serializer
  attribute :id
  attribute :name
end
