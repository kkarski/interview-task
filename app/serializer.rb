# frozen_string_literal: true

class Serializer
  attr_reader :object

  def self.attribute(name, &block)
    define_method(name) do
      return object.public_send(name) unless block_given?

      instance_eval(&block)
    end
  end

  def initialize(object)
    @object = object
  end

  def serialize
    object.members.each_with_object({}) do |k, hsh|
      hsh[k] = public_send(k) if instance_eval { public_methods(false).include?(k) }
    end
  end
end
