# frozen_string_literal: true

require_relative 'serializer'

class CarSerializer < Serializer
  attribute :id
  attribute :name
  attribute :created_at do
    object.created_at.strftime('%d-%m-%Y')
  end
end
