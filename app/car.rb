# frozen_string_literal: true

Car = Struct.new(:id, :name, :created_at)
